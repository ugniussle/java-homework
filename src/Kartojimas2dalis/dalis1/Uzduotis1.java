package Kartojimas2dalis.dalis1;

public class Uzduotis1 {
    public static void main(String[] args) {
        Point point1 = new Point(2, 2);
        System.out.println("Taškas 1: " + point1.toString());
        Point point2 = new Point(3, 4);
        System.out.println("Taškas 2: " + point2.toString());

        System.out.println();

        System.out.println("Taško 1 atstumas nuo (0, 0)");
        System.out.println(point1.distanceFromOrigin());

        System.out.println("Taško 2 atstumas nuo (0, 0)");
        System.out.println(point2.distanceFromOrigin());

        System.out.println();

        System.out.println("Taško 1 atstumas nuo Taško 2:");
        System.out.println(point1.distance(point2));

        System.out.println();

        System.out.println("Tašką 1 stumiame (-1;1):");
        point1.translate(-1, 1);
        System.out.println("Taškas 1: " + point1.toString());

        System.out.println();

        System.out.println("Taško 2 koordinates keičiame į (7;77):");
        point2.setLocation(7,77);
        System.out.println("Taškas 2: " + point2.toString());

        System.out.println();

        System.out.println("Taško 1 atstumas nuo Taško 2:");
        System.out.println(point1.distance(point2));
    }
}
