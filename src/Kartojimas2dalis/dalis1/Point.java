package Kartojimas2dalis.dalis1;

import java.util.Objects;

public class Point {
    /**
     * Distance of a point from origin in the x-axis.
     */
    double x;

    /**
     * Distance of a point from origin in the y-axis.
     */
    double y;

    /**
     * Constructor that sets x and y according
     * to parameters.
     */
    public Point(double x, double y) {
        this.setLocation(x, y);
    }

    /**
     * Returns x.
     */
    public double getX() {
        return x;
    }

    /**
     * Sets x to given parameter.
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * Returns y.
     */
    public double getY() {
        return y;
    }

    /**
     * Sets y to given parameter.
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * Calculates distance from [0; 0]
     *  to a specified point.
     */
    public double distanceFromOrigin() {
        return this.distance(0, 0);
    }

    /**
     * Modifies x and y by given parameters.
     * As in x += dx and y += dy
     *
     */
    public void translate(double dx, double dy) {
        this.setX(this.getX() + dx);
        this.setY(this.getY() + dy);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Point point)) return false;
        return getX() == point.getX() && getY() == point.getY();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY());
    }

    /**
     * Returns a string representation of
     *  this point in format "[x; y]"
     */
    @Override
    public String toString() {
        return String.format("[%.3f; %.3f]", this.getX(), this.getY());
    }

    /**
     * Calculates distance from this point
     *  to a specified point.
     */
    public double distance(Point point) {
        return this.distance(point.getX(), point.getY());
    }

    /**
    * Calculates distance from this point
    *  to a point at coordinates [_x; _y].
    */
    public double distance(double _x, double _y) {
        double x = this.getX() - _x;
        double y = this.getY() - _y;

        return Math.sqrt(x * x + y * y);
    }

    /**
     * Sets x and y to given parameters.
     */
    public void setLocation(double x, double y) {
        this.setX(x);
        this.setY(y);
    }
}
