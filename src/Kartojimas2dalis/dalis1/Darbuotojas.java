package Kartojimas2dalis.dalis1;

public class Darbuotojas {
    private String vardas;
    private String pavarde;
    private double atlyginimas;

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public void setPavarde(String pavarde) {
        this.pavarde = pavarde;
    }

    public double getAtlyginimas() {
        return atlyginimas;
    }

    public void setAtlyginimas(double atlyginimas) {
        this.atlyginimas = atlyginimas;
    }

    public Darbuotojas(String vardas, String pavarde, double atlyginimas) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.atlyginimas = atlyginimas;
    }

    public Darbuotojas() {
    }

    @Override
    public String toString() {
        return String.format("Vardas: %s\nPavardė: %s\nAtlyginimas: %.0f\n",
                this.vardas, this.pavarde, this.atlyginimas);
    }
}
