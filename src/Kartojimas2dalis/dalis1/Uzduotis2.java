package Kartojimas2dalis.dalis1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Uzduotis2 {
    public static double sodra = 0.09;
    public static double pajamu = 0.15;
    public static void main(String[] args) {
        Darbuotojas[] darbuotojai;

        try{
            darbuotojai = nuskaitytiDarbuotojuSarasa("src/Kartojimas2dalis/dalis1/uzduotis2.txt");
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            return;
        }

        for(Darbuotojas darbuotojas:darbuotojai) {
            System.out.println(darbuotojas.toString());
        }

        printInfo(darbuotojai);
    }

    public static double vidutinisUzmokestis(Darbuotojas[] darbuotojai) {
        if(darbuotojai.length == 0) {
            return 0.0;
        }

        double uzmokestis = 0.0;

        for(Darbuotojas darbuotojas:darbuotojai) {
            uzmokestis += darbuotojas.getAtlyginimas();
        }

        return uzmokestis /= darbuotojai.length;
    }

    public static double skaiciuotiSodrosMokesti(double atlyginimas) {
        return atlyginimas * sodra;
    }

    public static double skaiciuotiPajamuMokesti(double atlyginimas) {
        return atlyginimas * pajamu;
    }

    public static int gautiDarbuotojuSkaiciu(String failoVardas) throws FileNotFoundException {
        Scanner scanner = atidarytiFaila(failoVardas);

        int length = 0;

        while(scanner.hasNextLine()) {
            scanner.nextLine();
            length++;
        }

        scanner.close();

        return length;
    }

    public static Scanner atidarytiFaila(String failoVardas) throws FileNotFoundException {
        Scanner scanner;

        scanner = new Scanner(new File(failoVardas));

        return scanner;
    }

    public static Darbuotojas[] nuskaitytiDarbuotojuSarasa(String failoVardas) throws FileNotFoundException {
        int darbuotojuSkaicius = gautiDarbuotojuSkaiciu(failoVardas);

        Darbuotojas[] darbuotojai = new Darbuotojas[darbuotojuSkaicius];
        Scanner scanner = atidarytiFaila(failoVardas);

        for(int i = 0; i < darbuotojuSkaicius; i++) {
            String vardas = scanner.next();
            String pavarde = scanner.next();
            double atlyginimas = scanner.nextInt();

            Darbuotojas darbuotojas = new Darbuotojas(vardas, pavarde, atlyginimas);

            darbuotojai[i] = darbuotojas;
        }

        scanner.close();

        return darbuotojai;
    }

    public static void printInfo(Darbuotojas[] darbuotojai) {
        double darboUzmokestis = 0.0;
        double pajamuMokestciai = 0.0;
        double sodrosMokestciai = 0.0;

        for(Darbuotojas darbuotojas:darbuotojai) {
            double atlyginimas = darbuotojas.getAtlyginimas();
            double pajamuMokestis = skaiciuotiPajamuMokesti(atlyginimas);
            double sodrosMokestis = skaiciuotiSodrosMokesti(atlyginimas);

            darboUzmokestis += atlyginimas;
            pajamuMokestciai += pajamuMokestis;
            sodrosMokestciai += sodrosMokestis;
        }

        System.out.printf("Viso įmonėje darbuotojų: %d\n" +
                "Įmonė sumoka darbo užmokesčiui: %.1f\n" +
                "Sumokėtas pajamų mokestis: %.1f\n" +
                "Sumokėtas sodros mokestis: %.1f\n",
                darbuotojai.length, darboUzmokestis, pajamuMokestciai, sodrosMokestciai);
    }
}
