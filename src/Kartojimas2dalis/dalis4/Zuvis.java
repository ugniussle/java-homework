package Kartojimas2dalis.dalis4;

public class Zuvis extends Gyvunas{
    private int pelekuKiekis;

    public Zuvis(String name) {
        super(name, "Žuvis");
    }

    @Override
    public String toString() {
        return super.toString() + " Peleku kiekis: " + pelekuKiekis + "\n";
    }

    public int getPelekuKiekis() {
        return pelekuKiekis;
    }

    public void setPelekuKiekis(int pelekuKiekis) {
        this.pelekuKiekis = pelekuKiekis;
    }


}
