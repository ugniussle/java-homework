package Kartojimas2dalis.dalis4;

public class Ziurkenas extends Gyvunas {
    private int uodegosIlgis;
    private String spalva;

    public Ziurkenas(String name) {
        super(name, "Žiurkėnas");
    }

    public int getUodegosIlgis() {
        return uodegosIlgis;
    }

    public void setUodegosIlgis(int uodegosIlgis) {
        this.uodegosIlgis = uodegosIlgis;
    }

    @Override
    public String toString() {
        return super.toString() + " Uodegos Ilgis: " + uodegosIlgis + " cm, " +
                "Spalva: " + spalva + "\n";
    }

    public String getSpalva() {
        return spalva;
    }

    public void setSpalva(String spalva) {
        this.spalva = spalva;
    }
}
