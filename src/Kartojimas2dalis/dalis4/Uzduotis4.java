package Kartojimas2dalis.dalis4;

import java.util.Collections;
import java.util.LinkedList;

public class Uzduotis4 {
    public static void main(String[] args) {
        LinkedList<Gyvunas> gyvunai = new LinkedList<>();

        gyvunai.add(new Ziurkenas("Juozapas"));
        gyvunai.add(new Zuvis("Žuviukas"));
        gyvunai.add(new Ziurkenas("Tironas"));
        gyvunai.add(new Ziurkenas("Voldemortas"));
        gyvunai.add(new Zuvis("Rytis"));
        gyvunai.add(new Ziurkenas("Lebronas"));
        gyvunai.add(new Zuvis("Alfonsas"));

        Collections.sort(gyvunai);

        for(Gyvunas gyvunas:gyvunai) {
            System.out.print(gyvunas);
        }
    }
}
