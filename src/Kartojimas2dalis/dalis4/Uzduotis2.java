package Kartojimas2dalis.dalis4;

public class Uzduotis2 {
    public static void main(String[] args) {
        Ziurkenas ziurke = new Ziurkenas("Antanas");
        ziurke.setUodegosIlgis(10);
        ziurke.setSpalva("baltas");

        System.out.println(ziurke);

        Zuvis zuvele = new Zuvis("Aukse");
        zuvele.setPelekuKiekis(10);

        System.out.println(zuvele);
    }
}
