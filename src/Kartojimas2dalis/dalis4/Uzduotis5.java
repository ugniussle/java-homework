package Kartojimas2dalis.dalis4;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;

public class Uzduotis5 {
    static LinkedList<Gyvunas> gyvunai;

    public static void main(String[] args) {
        gyvunai = new LinkedList<>();

        gyvunai.add(new Ziurkenas("Juozapas"));
        gyvunai.add(new Zuvis("Žuviukas"));
        gyvunai.add(new Ziurkenas("Tironas"));
        gyvunai.add(new Ziurkenas("Voldemortas"));
        gyvunai.add(new Zuvis("Rytis"));
        gyvunai.add(new Ziurkenas("Lebronas"));
        gyvunai.add(new Zuvis("Lebronas"));
        gyvunai.add(new Zuvis("Alfonsas"));

        Collections.sort(gyvunai);

        Scanner scanner = new Scanner(System.in);

        while(true) {
            System.out.println("Įveskite vardą paieškai: ");

            String name = scanner.next();

            findGyvunas(name);
        }


    }

    public static void findGyvunas(String name){
        for(Gyvunas gyvunas:gyvunai) {
            if(gyvunas.name.equals(name)) {
                System.out.print(gyvunas);
            }
        }
    }
}
