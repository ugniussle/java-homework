package Kartojimas2dalis.dalis4;

public abstract class Gyvunas implements Comparable<Gyvunas> {
    protected String name;
    protected String type;

    @Override
    public int compareTo(Gyvunas gyvunas) throws NullPointerException {
        if(gyvunas == null) throw new NullPointerException("Compared object is null");

        if (this == gyvunas) return 0;

        if(!type.equals(gyvunas.type)) {
            return type.compareTo(gyvunas.type);
        } else {
            return name.compareTo(gyvunas.name);
        }
    }

    public Gyvunas(String name, String type) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String toString() {
        return type + ". " +
                "Vardas: " + name + ",";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
