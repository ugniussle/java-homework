package Kartojimas2dalis.dalis4;

import java.util.LinkedList;

public class Uzduotis3 {
    public static void main(String[] args) {
        LinkedList<Gyvunas> gyvunai = new LinkedList<>();

        gyvunai.add(new Ziurkenas("Juozapas"));
        gyvunai.add(new Zuvis("Žuviukas"));
        gyvunai.add(new Ziurkenas("Tironas"));
        gyvunai.add(new Ziurkenas("Voldemortas"));
        gyvunai.add(new Zuvis("Rytis"));
        gyvunai.add(new Ziurkenas("Lebronas"));
        gyvunai.add(new Zuvis("Alfonsas"));

        for(Gyvunas gyvunas:gyvunai) {
            System.out.print(gyvunas);
        }
    }
}
