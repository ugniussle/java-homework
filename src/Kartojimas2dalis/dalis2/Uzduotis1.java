package Kartojimas2dalis.dalis2;

import Kartojimas2dalis.dalis1.Point;

public class Uzduotis1 {
    public static void main(String[] args) {
        Point p1 = new Point(5, 5);
        Point p2 = new Point(12, 41);

        Line line = new Line(p1, p2);

        System.out.println(line.toString());
        System.out.printf("Linijos ilgis %.1f\n" , line.length());
    }
}
