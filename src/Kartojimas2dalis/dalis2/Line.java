package Kartojimas2dalis.dalis2;

import Kartojimas2dalis.dalis1.Point;

public class Line {
    Point start;
    Point end;

    public Point getStart() {
        return start;
    }

    public void setStart(Point start) {
        this.start = start;
    }

    public Point getEnd() {
        return end;
    }

    public void setEnd(Point end) {
        this.end = end;
    }

    public Line() {
    }

    public Line(Point start, Point end) {
        this.start = start;
        this.end = end;
    }

    public double length() {
        return start.distance(end);
    }

    @Override
    public String toString() {
        return "Line{" +
                "start=" + start +
                ", end=" + end +
                String.format(", distance=%.2f", this.length()) +
                "}";
    }
}
