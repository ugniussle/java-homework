package Kartojimas2dalis.dalis3;

public class Hourly extends Employee{
    private int hoursWorked = 0;
    public Hourly(String name, String surname, String phone, int insuranceId, int hourlyRate) {
        super(name, surname, phone, insuranceId, hourlyRate);
    }

    public void addHours(int hours) {
        this.hoursWorked += hours;
    }
    @Override
    public double pay() {
        double pay = hoursWorked * wage;
        hoursWorked = 0;
        return pay;
    }
}
