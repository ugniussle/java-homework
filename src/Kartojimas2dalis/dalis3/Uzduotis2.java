package Kartojimas2dalis.dalis3;

public class Uzduotis2 {
    public static void main(String[] args) {
        StaffMember[] staffMembers = new StaffMember[6];

        Employee employee1 = new Employee("Antanas", "Antanaitis", "+37068497864", 10000000, 2000);
        Employee employee2 = new Employee("Bronius", "Bronaitis", "+37068745685", 10000001, 1500);
        Trainee trainee = new Trainee("Pinigis", "Bepinigis", "+37068349735");
        Executive executive = new Executive("Juozas", "Juozaitis", "+37066541658", 10000002, 4000);
        Hourly hourly1 = new Hourly("Tomas", "Tominskas", "+37066484521", 10000003, 10);
        Hourly hourly2 = new Hourly("Darius", "Dorys", "+37066541687", 10000004, 10);

        executive.awardBonus(150);

        hourly1.addHours(10);
        hourly2.addHours(20);

        staffMembers[0] = employee1;
        staffMembers[1] = employee2;
        staffMembers[2] = trainee;
        staffMembers[3] = executive;
        staffMembers[4] = hourly1;
        staffMembers[5] = hourly2;

        Staff staff = new Staff(staffMembers);

        System.out.println(staff.getMonthlyReport());
    }
}
