package Kartojimas2dalis.dalis3;

public class Executive extends Employee{
    private double bonus = 0;
    public Executive(String name, String surname, String phone, int insuranceId, int wage) {
        super(name, surname, phone, insuranceId, wage);
    }

    public void awardBonus(double bonus) {
        this.bonus += bonus;
    }

    @Override
    public double pay() {
        return super.pay() + bonus;
    }
}
