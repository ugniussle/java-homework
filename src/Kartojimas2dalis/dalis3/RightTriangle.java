package Kartojimas2dalis.dalis3;

import Kartojimas2dalis.dalis1.Point;
import Kartojimas2dalis.dalis2.Line;


public class RightTriangle extends Triangle {
    public RightTriangle(Line a, Line b) {
        super();

        Line c = new Line();

        Point aStart = a.getStart(), aEnd=a.getEnd(),
                bStart = b.getStart(), bEnd=b.getEnd();

        if(aStart.equals(bStart)) {
            c.setStart(aEnd);
            c.setEnd(bEnd);
        }
        if(aStart.equals(bEnd)) {
            c.setStart(aEnd);
            c.setEnd(bStart);
        }
        if(aEnd.equals(bStart)) {
            c.setStart(aStart);
            c.setEnd(bEnd);
        }
        if(aEnd.equals(bEnd)) {
            c.setStart(aStart);
            c.setEnd(bStart);
        }

        this.setA(a);
        this.setB(b);
        this.setC(c);
    }
}
