package Kartojimas2dalis.dalis3;

public class Employee extends StaffMember{
    int insuranceId;
    int wage;

    public Employee(String name, String surname, String phone, int insuranceId, int wage) {
        super(name, surname, phone);
        this.insuranceId = insuranceId;
        this.wage = wage;
    }

    @Override
    public double pay() {
        return wage;
    }
}
