package Kartojimas2dalis.dalis3;

public class Staff {
    StaffMember[] staff;

    public Staff(StaffMember[] staff) {
        this.staff = staff;
    }

    public StaffMember[] getStaff() {
        return staff;
    }

    public String getMonthlyReport() {
        String report = "";

        double totalPay = 0;
        for(StaffMember member:staff) {
            double pay = member.pay();

            report += member.toString() + " wage: " + pay + "\n";
            totalPay += pay;
        }

        report += "Total wages: " + totalPay;

        return report;
    }
}
