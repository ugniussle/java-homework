package Kartojimas2dalis.dalis3;

import Kartojimas2dalis.dalis1.Point;
import Kartojimas2dalis.dalis2.Line;

public class Uzduotis1 {
    public static void main(String[] args) {
        Line a = new Line(new Point(1, 1), new Point(0,1));
        Line b = new Line(new Point(0, 0), new Point(1, 1));

        RightTriangle rightTriangle = new RightTriangle(a, b);

        System.out.println(rightTriangle.toString());

        a = new Line(new Point(1, 1), new Point(5, 7));
        EquilateralTriangle equilateralTriangle = new EquilateralTriangle(a);

        System.out.println(equilateralTriangle.toString());
    }


}
