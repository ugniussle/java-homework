package Kartojimas2dalis.dalis3;

public abstract class StaffMember {
    protected String name;
    protected String surname;
    protected String phone;

    public StaffMember(String name, String surname, String phone) {
        this.name = name;
        this.surname = surname;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "StaffMember{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    public abstract double pay();
}
