package Kartojimas2dalis.dalis3;

import Kartojimas2dalis.dalis1.Point;
import Kartojimas2dalis.dalis2.Line;
public class EquilateralTriangle extends Triangle {
    public EquilateralTriangle(Line c) {
        Line a = new Line(), b = new Line();

        Point A = c.getStart(), B = c.getEnd(), C = getThirdPoint(c);

        a.setStart(B); a.setEnd(C);
        b.setStart(A); b.setEnd(C);

        this.a = a;
        this.b = b;
        this.c = c;
    }

    private static Point getThirdPoint(Line line) {
        Point point = new Point(
                line.getEnd().getX() - line.getStart().getX(),
                line.getEnd().getY() - line.getStart().getY()
        );

        Point thirdPoint = new Point(
                point.getX() * Math.cos(Math.toRadians(60)) - point.getY() * Math.sin(Math.toRadians(60)) + line.getStart().getX(),
                point.getX() * Math.sin(Math.toRadians(60)) + point.getY() * Math.cos(Math.toRadians(60)) + line.getStart().getY()
        );

        return thirdPoint;
    }
}
