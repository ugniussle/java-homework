package Kartojimas2dalis.dalis3;


import Kartojimas2dalis.dalis2.Line;

public class Triangle {
    Line a, b, c;

    public Triangle() {
    }

    public void setA(Line a) {
        this.a = a;
    }

    public void setB(Line b) {
        this.b = b;
    }

    public void setC(Line c) {
        this.c = c;
    }

    public Triangle(Line a, Line b, Line c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "\na=" + a +
                ",\nb=" + b +
                ",\nc=" + c +
                '}';
    }

    public double area() {
        double aL, bL, cL;

        aL = a.length();
        bL = b.length();
        cL = c.length();

        double p = (aL + bL + cL) / 2; // (a+b+c)/2

        return Math.sqrt(p*(p-aL)*(p-bL)*(p-cL)); // Math.sqrt(p*(p-a)*(p-b)*(p-c))
    }
}
