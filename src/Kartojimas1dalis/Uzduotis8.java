package Kartojimas1dalis;

public class Uzduotis8 {
    public static void main(String[] args) {
        round(10.3344445, 4);
        round(1.22, 1);
        round(1.22, 4);
        round(1000.10001, 0);
        round(5.12344445, 3);
        round(5.19844445, 3);
    }

    public static double round(double number, int precision) {
        int[] digitsAfterDot = new int[15];

        double mantissa = Math.floor(number);

        double decimal = number % 1;

        // susirąšome skaičius po kablelio į masyvą
        for(int i = 0; i < digitsAfterDot.length; i++) {
            decimal *= 10;
            digitsAfterDot[i] = (int)Math.floor(decimal%10);

            //System.out.print(digitsAfterDot[i]);
        }

        // modifikuojame skaičių po kablelio masyvą pagal tikslumą ir ar galima supavalinti į viršų
        boolean canRoundUp = false;
        for(int i = 0; i < digitsAfterDot.length; i++) {
            if(i == precision) {
                if(digitsAfterDot[i] == 4) {
                    //System.out.println("can round up");
                    canRoundUp = true;
                }

                if(digitsAfterDot[i] >= 5) {
                    digitsAfterDot[precision-1]++;
                    digitsAfterDot[i] = 0;
                }
            }

            if(canRoundUp) {
                if(digitsAfterDot[i] < 4) {
                    canRoundUp = false;
                }

                if(digitsAfterDot[i] > 4) {
                    digitsAfterDot[precision-1]++;
                    canRoundUp = false;
                }
            }

            if(i >= precision) {
                digitsAfterDot[i] = 0;
            }
        }

        // konvertuojame masyvą į skaičius po kablelio
        double newDecimal = 0.0;
        for(int i = digitsAfterDot.length-1; i >= 0; i--) {
            newDecimal += digitsAfterDot[i];
            newDecimal /= 10;
            //System.out.printf("%d | %f\n", digitsAfterDot[i], newDecimal);
        }

        System.out.printf("round(%.7f, %d)", number, precision);
        System.out.printf(" -> %.7f\n", mantissa + newDecimal);

        return mantissa + newDecimal;
    }
}
