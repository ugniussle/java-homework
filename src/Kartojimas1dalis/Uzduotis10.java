package Kartojimas1dalis;

import java.io.File;
import java.util.Scanner;

public class Uzduotis10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while(true){
            try{
                System.out.println("Įveskite failo pavadinimą: ");
                String filename = scanner.nextLine();

                System.out.println("Įveskite direktoriją: ");
                String path = scanner.nextLine();

                printRecursiveDir(path, filename);

                break;
            } catch(java.lang.NullPointerException e) {
                System.out.println(e.getMessage());
                System.out.println("Bandykite dar kartą.");
            }
        }
    }

    private static void printRecursiveDir(String parentPath, String filename) {
        File parentFile = new File(parentPath);

        for(File childFile : parentFile.listFiles()) {
            //System.out.println(childFile.getAbsolutePath());

            if(childFile.getName().equals(filename)) {
                System.out.println(childFile.getAbsolutePath());
            }

            if(childFile.isDirectory()) {
                printRecursiveDir(childFile.getAbsolutePath(), filename);
            }
        }
    }
}
