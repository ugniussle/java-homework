package Kartojimas1dalis;

import java.util.Scanner;

public class Uzduotis4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Įveskite skaičių: ");

        int number = scanner.nextInt();

        int length = getNumberLength(number);

        int[] digits = new int[length];

        int n = number;

        for(int i = 0; i < length; i++) {
            digits[i] = n % 10;
            n /= 10;
        }

        int odd = 0, even = 0;

        for(int digit : digits) {
            if(digit % 2 == 0) {
                even++;
            } else {
                odd++;
            }
        }

        System.out.printf("Skaičiaus %d\nLyginiai skaičiai: %d\nNelyginiai skaičiai: %d\n",
                number, even, odd);
    }

    public static int getNumberLength(int number) {
        int length = 0;

        while(number != 0) {
            number /= 10;
            length++;
        }

        return length;
    }
}
