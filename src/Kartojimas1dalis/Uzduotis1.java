package Kartojimas1dalis;

import java.util.Scanner;

public class Uzduotis1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Įveskite trikampio kraštines: ");

        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        double c = scanner.nextDouble();

        double p = (a+b+c)/2;

        double area = Math.sqrt(p*(p-a)*(p-b)*(p-c));

        System.out.printf("Trikampio plotas: %.2f\n", area);
    }
}
