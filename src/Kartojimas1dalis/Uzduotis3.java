package Kartojimas1dalis;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Uzduotis3 {
    public static void main(String[] args) {
        Scanner scanner;

        try{
            scanner = new Scanner(new File("src/Kartojimas1dalis/uzduotis3.txt"));
        } catch (FileNotFoundException e) {
            System.out.printf(e.getMessage());
            return;
        }

        int n = scanner.nextInt();

        for(int i = 1; i < n; i++) {
            if(i < 100) {
                continue;
            }

            if (i > 999) {
                break;
            }

            int digit1 = (i / 100) % 10;
            int digit2 = (i / 10) % 10;
            int digit3 = i % 10;

            if(digit1*digit1*digit1 + digit2*digit2*digit2 + digit3*digit3*digit3 == i) {
                System.out.printf("Skaičius %d yra Armstrongo skaičius.\n", i);
            }
        }
    }
}
