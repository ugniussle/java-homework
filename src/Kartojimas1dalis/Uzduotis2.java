package Kartojimas1dalis;

import java.util.Scanner;

public class Uzduotis2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Įveskite metus: ");

        int year = scanner.nextInt();

        if((year % 4 == 0 && year % 100 != 0) || (year % 100 == 0 && year % 400 == 0)) {
            System.out.println("Metai yra keliamieji.");
        } else {
            System.out.println("Metai nėra keliamieji.");
        }
    }
}
