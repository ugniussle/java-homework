package Kartojimas1dalis;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Uzduotis5 {
    public static void main(String[] args) {
        Scanner scanner;

        try{
            scanner = new Scanner(new File("src/Kartojimas1dalis/uzduotis5.txt"));
        } catch(FileNotFoundException e) {
            System.out.println(e.getMessage());
            return;
        }

        int[] digitCount = new int[10];

        while(scanner.hasNext()) {
            int digit = scanner.nextInt();

            digitCount[digit]++;
        }

        for(int i = 0; i < 10; i++) {
            if(digitCount[i] > 0) {
                System.out.printf("Skaičius %d pasikartojo %d kartų.\n", i, digitCount[i]);
            }
        }
    }
}
