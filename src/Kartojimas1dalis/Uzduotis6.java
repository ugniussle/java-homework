package Kartojimas1dalis;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Uzduotis6 {
    public static void main(String[] args) {
        Scanner scanner;

        int numberCount = getFileNumberCount("src/Kartojimas1dalis/uzduotis6.txt");

        try{
            scanner = new Scanner(new File("src/Kartojimas1dalis/uzduotis6.txt"));
        } catch(FileNotFoundException e) {
            System.out.println(e.getMessage());
            return;
        }

        float[] numbers = new float[numberCount];

        for(int i = 0; scanner.hasNext(); i++) {
            numbers[i] = scanner.nextFloat();
        }

        float sum = 0;
        for(int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }

        System.out.print("Skaičiai: ");
        System.out.println(Arrays.toString(numbers));
        System.out.printf("Vidurkis: %.2f\n", sum/(float)numberCount);

        if(numberCount%2 == 0) {
            System.out.printf("Mediana: %.2f\n", (numbers[numberCount/2] + numbers[(numberCount+1)/2])/2);
        } else {
            System.out.printf("Mediana: %.1f\n", numbers[numberCount/2]);
        }
    }

    public static int getFileNumberCount(String filename) {
        Scanner scanner;

        try{
            scanner = new Scanner(new File(filename));
        } catch(FileNotFoundException e) {
            System.out.println(e.getMessage());
            return -1;
        }

        int length = 0;

        while(scanner.hasNext()) {
            scanner.next();
            length++;
        }

        scanner.close();

        return length;
    }
}
