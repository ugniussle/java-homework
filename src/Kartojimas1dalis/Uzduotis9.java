package Kartojimas1dalis;

import java.util.Scanner;

public class Uzduotis9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Įveskite skaičių: ");
        int number = scanner.nextInt();

        int digitCount = 0;

        int n = number;
        while(n != 0) {
            digitCount++;
            n /= 10;
        }

        int reversedNumber = 0;
        n = number;
        while(n != 0) {
            reversedNumber += n % 10 * Math.pow(10, digitCount-1);
            n /= 10;
            digitCount--;
        }

        System.out.printf("%d apverstas yra %d", number, reversedNumber);
    }
}
