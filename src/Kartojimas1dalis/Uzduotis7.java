package Kartojimas1dalis;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Uzduotis7 {
    public static void main(String[] args) {
        Scanner scanner;

        try{
            scanner = new Scanner(new File("src/Kartojimas1dalis/uzduotis7.txt"));
        } catch(FileNotFoundException e) {
            System.out.println(e.getMessage());
            return;
        }

        int subjectCount = scanner.nextInt();
        int studentCount = scanner.nextInt();

        int[][] studentsGrades = readStudentsGrades(studentCount, subjectCount, scanner);

        float[] studentsAverages = getStudentAverages(studentCount, subjectCount, studentsGrades);

        float groupAverage = getGroupAverage(studentCount, studentsAverages);

        int bestStudent = getBestStudent(studentCount, studentsAverages);

        boolean[] studentStatus = getStudentStatus(studentCount, subjectCount, studentsGrades);

        System.out.printf("%14s", "Dalykai:");

        for(int j = 0; j < subjectCount; j++) {
            System.out.printf("%3d", j+1);
        }
        System.out.println();

        for(int i = 0; i < studentCount; i++) {
            System.out.printf("%9s %3d:", "Studentas", i);

            for(int j = 0; j < subjectCount; j++) {
                System.out.printf("%3d", studentsGrades[i][j]);
            }

            System.out.println();
        }

        System.out.println();

        for(int i = 0; i < studentCount; i++) {
            System.out.printf("Studento %2d vidurkis: %.2f", i, studentsAverages[i]);

            System.out.println();
        }
        System.out.println();

        System.out.printf("Grupės vidurkis: %.2f\n", groupAverage);
        System.out.println();

        System.out.printf("Geriausiai besimokantis studentas: %d\n", bestStudent);
        System.out.println();

        System.out.printf("Nepažangūs studentai: ");

        boolean first = true;
        for(int i = 0; i < studentCount; i++) {
            if(studentStatus[i] == false && !first) {
                System.out.printf(", %d", i);
            }

            if(studentStatus[i] == false && first) {
                System.out.printf("%d", i);
                first = false;
            }
        }
    }

    public static int[][] readStudentsGrades(int studentCount, int subjectCount, Scanner scanner) {
        int[][] studentsGrades = new int[studentCount][subjectCount];

        for(int i = 0; i < studentCount; i++) {
            for(int j = 0; j < subjectCount; j++) {
                studentsGrades[i][j] = scanner.nextInt();
            }
        }

        return studentsGrades;
    }

    public static float[] getStudentAverages(int studentCount, int subjectCount, int[][] studentsGrades) {
        float[] studentsAverages = new float[studentCount];

        for(int i = 0; i < studentCount; i++) {
            for(int j = 0; j < subjectCount; j++) {
                studentsAverages[i] += studentsGrades[i][j];
            }

            studentsAverages[i] /= subjectCount;
        }

        return studentsAverages;
    }

    public static float getGroupAverage(int studentCount, float[] studentsAverages) {
        float groupAverage = 0;

        for(int i = 0; i < studentCount; i++) {
            groupAverage += studentsAverages[i];
        }

        groupAverage /= studentCount;

        return groupAverage;
    }

    public static int getBestStudent(int studentCount, float[]studentsAverages) {
        int bestStudent = 0;
        float highest = studentsAverages[0];

        for(int i = 1; i < studentCount; i++) {
            if(studentsAverages[i] > highest) {
                highest = studentsAverages[i];
                bestStudent = i;
            }
        }

        return bestStudent;
    }

    public static boolean[] getStudentStatus(int studentCount, int subjectCount, int[][]studentsGrades) {
        boolean[] studentStatus = new boolean[studentCount];

        for(int i = 0; i < studentCount; i++) {
            studentStatus[i] = true;
            for(int j = 0; j < subjectCount; j++) {
                if(studentsGrades[i][j] < 5) {
                    studentStatus[i] = false;
                }
            }
        }

        return studentStatus;
    }
}
