package Kartojimas3dalis;

import java.util.LinkedList;
import java.util.Scanner;

public class Uzduotis3 {
    public static void main(String[] args) {
        PhoneNumberDatabase phoneNumbers = new PhoneNumberDatabase();

        phoneNumbers.addNumber("Jonas", "+37064862153");
        phoneNumbers.addNumber("Jonas", "+37061111111");
        phoneNumbers.addNumber("Jonas", "+37062222222");
        phoneNumbers.addNumber("Antanas", "+37062244343");
        phoneNumbers.addNumber("Antanas", "+37062222224");

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println();
            System.out.println("1 - peržiūrėti adresatus");
            System.out.println("2 - pridėti adresatą");

            int choice = scanner.nextInt();

            if(choice == 1) {
                System.out.println("Pasirinkite adresatą: ");

                System.out.println(phoneNumbers.keysToString());

                String name = scanner.next();

                if(phoneNumbers.phoneNumbers.containsKey(name)) {
                    while(true) {
                        System.out.println("Adresato " + name + " numeriai: ");

                        System.out.println(phoneNumbers.valuesToString(name));

                        System.out.println();
                        System.out.println("1 - pridėti numerį");
                        System.out.println("2 - eiti atgal");

                        choice = scanner.nextInt();

                        if(choice == 1) {
                            System.out.print("Įveskite naują adresato numerį: ");
                            String newNumber = scanner.next();

                            if(!newNumber.isEmpty()) {
                                phoneNumbers.addNumber(name, newNumber);
                            } else {
                                System.out.println("Klaida, tuščias numeris.");
                            }
                        } else if (choice == 2) {
                            break;
                        }
                    }
                } else {
                    System.out.println("Toks adresatas neegzistuoja.");
                }
            } else if (choice == 2) {
                System.out.println("Įveskite adresato vardą: ");

                String name = scanner.next();

                if(phoneNumbers.phoneNumbers.containsKey(name)) {
                    System.out.println("Toks adresatas jau egzistuoja.");
                } else {
                    phoneNumbers.phoneNumbers.put(name, new LinkedList<>());
                }
            }
        }
    }
}
