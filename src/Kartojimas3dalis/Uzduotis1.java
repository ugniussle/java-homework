package Kartojimas3dalis;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Uzduotis1 {

    public static void main(String[] args) {
        List<Employee> darbuotojai = new ArrayList<>(10);

        Scanner scanner;

        try{
            scanner = new Scanner(new File("src/Kartojimas3dalis/uzduotis1.txt"));
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            return;
        }


        while(scanner.hasNext()) {
            Employee darbuotojas = new Employee();

            darbuotojas.setVardas(scanner.next());
            darbuotojas.setPavarde(scanner.next());
            darbuotojas.setElpastas(scanner.next());
            darbuotojas.setTelefonoNr(scanner.next());
            darbuotojas.setAtlyginimas(scanner.nextInt());

            darbuotojai.add(darbuotojas);
        }

        System.out.println(darbuotojai);

        Collections.sort(darbuotojai);

        System.out.println(darbuotojai);

        int visasUzmokestis = 0;

        for(Employee darbuotojas:darbuotojai) {
            visasUzmokestis += darbuotojas.getAtlyginimas();
        }

        System.out.println("Visas darbo užmokėstis: " + visasUzmokestis);
    }

}
