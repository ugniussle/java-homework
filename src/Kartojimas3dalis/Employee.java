package Kartojimas3dalis;

public class Employee implements Comparable<Employee> {
    private String vardas;
    private String pavarde;
    private String elpastas;
    private String telefonoNr;
    private int atlyginimas;

    public Employee(String vardas, String pavarde, String elpastas, String telefonoNr, int atlyginimas) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.elpastas = elpastas;
        this.telefonoNr = telefonoNr;
        this.atlyginimas = atlyginimas;
    }

    public Employee() {
    }

    @Override
    public String toString() {
        return "Employee{" +
                "vardas='" + vardas + '\'' +
                ", pavarde='" + pavarde + '\'' +
                ", elpastas='" + elpastas + '\'' +
                ", telefonoNr='" + telefonoNr + '\'' +
                ", atlyginimas=" + atlyginimas +
                "}\n";
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public void setPavarde(String pavarde) {
        this.pavarde = pavarde;
    }

    public String getElpastas() {
        return elpastas;
    }

    public void setElpastas(String elpastas) {
        this.elpastas = elpastas;
    }

    public String getTelefonoNr() {
        return telefonoNr;
    }

    public void setTelefonoNr(String telefonoNr) {
        this.telefonoNr = telefonoNr;
    }

    public int getAtlyginimas() {
        return atlyginimas;
    }

    public void setAtlyginimas(int atlyginimas) {
        this.atlyginimas = atlyginimas;
    }

    @Override
    public int compareTo(Employee employee) {
        return this.pavarde.compareTo(employee.getPavarde());
    }
}
