package Kartojimas3dalis;

import java.util.Scanner;

public class Uzduotis2 {
    public static void main(String[] args) {
        NumberGuessingGame game = new NumberGuessingGame(100);

        System.out.println("Spėkite skaičių tarp 1 ir 100: ");

        Scanner scanner = new Scanner(System.in);

        while(!game.guess(scanner.nextInt())) {}
    }
}
