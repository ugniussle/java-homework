package Kartojimas3dalis;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class PhoneNumberDatabase {
    Map<String, LinkedList<String>> phoneNumbers;

    public PhoneNumberDatabase() {
        phoneNumbers = new HashMap<>();
    }

    @Override
    public String toString() {
        String string = "";

        for(String key : phoneNumbers.keySet()) {
            string += key + " : " + phoneNumbers.get(key) + "\n";
        }

        return string;
    }

    public String keysToString() {
        String string = "";

        for(String key : phoneNumbers.keySet()) {
            string += key + "\n";
        }

        return string;
    }

    public String valuesToString(String key) {
        String string = "";

        string += phoneNumbers.get(key);

        return string;
    }

    public void addNumber(String name, String phoneNumber) {
        if(!phoneNumbers.containsKey(name)) {
            phoneNumbers.put(name, new LinkedList<>());
        }

        phoneNumbers.get(name).add(phoneNumber);
    }
}
