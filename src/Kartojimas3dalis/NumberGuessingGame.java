package Kartojimas3dalis;

import java.util.HashSet;
import java.util.Set;

public class NumberGuessingGame {
    private int  number;
    private Set<Integer> guesses;

    public NumberGuessingGame(int limit) {
        this.number = (int)(Math.random() * limit + 1);

        guesses = new HashSet<>();
    }

    public boolean guess(int number) {
        System.out.printf("Jūs spėjote %d.\n", number);
        if(number == this.number) {
            System.out.println("Spėjimas teisingas.");
            return true;
        } else {
            if(guesses.contains(number)) {
                System.out.println("Šiuo skaičiu jau spėjote. Bandykite dar kartą.");
            } else {
                guesses.add(number);
                if(number > this.number) {
                    System.out.println("Jūsų skaičius per didelis. Bandykite dar kartą.");
                } else {
                    System.out.println("Jūsų skaičius per mažas. Bandykite dar kartą.");
                }
            }

            return false;
        }
    }
}
